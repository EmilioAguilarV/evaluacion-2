<%-- 
    Document   : RegistroActivo
    Created on : 18-10-2020, 16:35:36
    Author     : Mclio
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="root.entities.AtActivo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<AtActivo> usuarios = (List<AtActivo>) request.getAttribute("activos");
    Iterator<AtActivo> itAtActivo = usuarios.iterator();
%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <title>Ingreso de activo</title>
    </head>
    <body class="text-center" >
        <form name="form" action="MantencionActivos" method="POST">  
   
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <table border="1">
                    <thead>
                    <th>id</th>
                    <th>nombre </th>
                    <th> </th>
                    </thead>
                    <tbody>
                        <%while (itAtActivo.hasNext()) {
                       AtActivo usu = itAtActivo.next();%>
                        <tr>
                            <td><%= usu.getId()%></td>
                            <td><%= usu.getNombre()%></td>
                          

                            <td> <input type="radio" name="seleccion" value="<%= usu.getId()%>"> </td>
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Editar</button>
                <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Eliminar</button>
                <button type="submit" name="accion"  value="Ingresar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Ingresar</button>

        </form>
    </body>
</html>