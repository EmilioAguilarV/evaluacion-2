/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.dao.exceptions.NonexistentEntityException;
import com.dao.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.entities.AtActivo;

/**
 *
 * @author Mclio
 */
public class AtActivoJpaController implements Serializable {
private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
   
public AtActivoJpaController() {
        this.emf = emf;
    }
    

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AtActivo atActivo) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(atActivo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAtActivo(atActivo.getId()) != null) {
                throw new PreexistingEntityException("AtActivo " + atActivo + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AtActivo atActivo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            atActivo = em.merge(atActivo);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = atActivo.getId();
                if (findAtActivo(id) == null) {
                    throw new NonexistentEntityException("The atActivo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AtActivo atActivo;
            try {
                atActivo = em.getReference(AtActivo.class, id);
                atActivo.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The atActivo with id " + id + " no longer exists.", enfe);
            }
            em.remove(atActivo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AtActivo> findAtActivoEntities() {
        return findAtActivoEntities(true, -1, -1);
    }

    public List<AtActivo> findAtActivoEntities(int maxResults, int firstResult) {
        return findAtActivoEntities(false, maxResults, firstResult);
    }

    private List<AtActivo> findAtActivoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AtActivo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AtActivo findAtActivo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AtActivo.class, id);
        } finally {
            em.close();
        }
    }

    public int getAtActivoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AtActivo> rt = cq.from(AtActivo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
