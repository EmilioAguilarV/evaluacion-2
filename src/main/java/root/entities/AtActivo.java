/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Mclio
 */
@Entity
@Table(name = "at_activo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AtActivo.findAll", query = "SELECT a FROM AtActivo a"),
    @NamedQuery(name = "AtActivo.findById", query = "SELECT a FROM AtActivo a WHERE a.id = :id"),
    @NamedQuery(name = "AtActivo.findByNombre", query = "SELECT a FROM AtActivo a WHERE a.nombre = :nombre")})
public class AtActivo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;

    public AtActivo() {
    }

    public AtActivo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AtActivo)) {
            return false;
        }
        AtActivo other = (AtActivo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entities.AtActivo[ id=" + id + " ]";
    }
    
}
