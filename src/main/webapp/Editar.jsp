<%-- 
    Document   : Editar
    Created on : 18-10-2020, 18:26:54
    Author     : Mclio
--%>

<%@page import="root.entities.AtActivo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<%
  AtActivo activo=(AtActivo)request.getAttribute("activos");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="MantencionActivos" method="POST">
                    <div class="form-group">
                        <label for="id">ID</label>
                        <input  name="id" class="form-control"  value="<%= activo.getId()%>"  required id="rut" >
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input  step="any" name="nombre" class="form-control"  value="<%= activo.getNombre()%>"  required id="nombre" aria-describedby="nombreHelp">
                     </div>       
                    <br>
                    <br>
                    <button type="submit" name="accion" value="update" class="btn btn-success">Grabar</button>
                           <button type="submit" class="btn btn-success">Salir</button>
                </form>
    </body>
</html>
