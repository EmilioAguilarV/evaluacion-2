/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.dao.AtActivoJpaController;
import com.dao.exceptions.NonexistentEntityException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.entities.AtActivo;

/**
 *
 * @author Mclio
 */
@WebServlet(urlPatterns = {"/MantencionActivos"})
public class MantencionActivos extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MantencionActivos</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MantencionActivos at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    String  accion =  request.getParameter("accion");
      if (accion.equals("Ingresar")){
             request.getRequestDispatcher("NewActivo.jsp").forward(request, response);
      }
      if (accion.equals("grabar")){
          
            String nombre = request.getParameter("nombre");
            int id = Integer.parseInt(request.getParameter("id"));
          
            System.out.println("nombre "+nombre);
            System.out.println("id " +id);
            //Crear Entitties y llamar dao.
          AtActivoJpaController dao=new AtActivoJpaController();
          
        AtActivo nactivo = new AtActivo();
          nactivo.setNombre(nombre);
          nactivo.setId(id);
          
        try {
            dao.create(nactivo);
        } catch (Exception ex) {
            System.out.println("Error " );
            
            Logger.getLogger(MantencionActivos.class.getName()).log(Level.SEVERE, null, ex);
        }
          
       List<AtActivo> activos = dao.findAtActivoEntities();
             request.setAttribute("activos", activos);
        request.getRequestDispatcher("RegistroActivo.jsp").forward(request, response);
      }
      if (accion.equals("eliminar")) {

            int seleccion = Integer.parseInt(request.getParameter("seleccion"));

            AtActivoJpaController dao = new AtActivoJpaController();
            try {
                dao.destroy(seleccion);
            } catch (NonexistentEntityException ex) {
                System.out.println("error al eliminar" + seleccion);
                Logger.getLogger(MantencionActivos.class.getName()).log(Level.SEVERE, null, ex);
            }
           List<AtActivo> activos = dao.findAtActivoEntities();
        request.setAttribute("activos", activos);
        request.getRequestDispatcher("RegistroActivo.jsp").forward(request, response);     

        }
       if (accion.equals("editar")) {
            String seleccion = request.getParameter("seleccion");
            if ((seleccion != null) && (!seleccion.equals(""))) {

                AtActivoJpaController dao = new AtActivoJpaController();

                AtActivo activo = dao.findAtActivo(Integer.parseInt(seleccion));

                request.setAttribute("activo", activo);
                request.getRequestDispatcher("Editar.jsp").forward(request, response);
            } else {
                AtActivoJpaController dao = new AtActivoJpaController();
                 List<AtActivo> activos = dao.findAtActivoEntities();
        request.setAttribute("activos", activos);
        request.getRequestDispatcher("RegistroActivo.jsp").forward(request, response);

            }

        }
      if (accion.equals("update")) {

            int id = Integer.parseInt(request.getParameter("id"));
            String nombre = request.getParameter("nombre");

            AtActivo activo = new AtActivo();
            activo.getId();
            activo.getNombre();

            AtActivoJpaController dao = new AtActivoJpaController();
            try {
                dao.edit(activo);
            } catch (Exception ex) {
                Logger.getLogger(MantencionActivos.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<AtActivo> activos = dao.findAtActivoEntities();
        request.setAttribute("activos", activos);
        request.getRequestDispatcher("RegistroActivo.jsp").forward(request, response);

        }
       
       
       
      
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
